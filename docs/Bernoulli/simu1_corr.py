from random import random

def simulation_loi() :
    x = random()
    if x < 0.2 :
        return -1
    elif x < 0.5 : #elif : sinon si
        return 0
    elif x < 0.75 :
        return 1
    elif x < 0.9 :
        return 2
    else :
        return 3


# Tests
assert simulation_loi() in {-1,0,1,2,3}

