# Loi de probabilité - Variable aléatoire

## Un peu d'Histoire

??? note "Le saviez-vous ?"
    ![JBernoulli](../images/JBernoulli.png){align=left width=30%} 
    La parution de l'Ars Conjectandi de [Jacques Bernoulli](http://bibmath.net/bios/index.php?action=affiche&quoi=bernoullijacob){target=_blank} (1713), reprenant notamment d'anciens travaux de Huygens, marque une rupture 
    dans l'histoire des probabilités. On y trouve la première étude de la distribution binomiale, introduite dans le cadre d'un tirage sans remise 
    pour un modèle d'urne.  
    Un résultat majeur de cet ouvrage est son « théorème d'or », la loi des grands nombres, qui relie fréquences et probabilité, valide le principe 
    de l'échantillonnage et est le premier exemple de « théorème limite » en théorie des probabilités. 

    Le mathématicien français [Bienaymé](http://bibmath.net/bios/index.php?action=affiche&quoi=bienayme){target=_blank}  (en 1853, publication en 1867) et le mathématicien russe [Tchebychev](https://fr.wikipedia.org/wiki/Pafnouti_Tchebychev){target=_blank} (en 1867) démontrent l'inégalité qui porte leur nom, en parlant de fréquences d'échantillons plutôt que de variables aléatoires. Ils fournissent ainsi la possibilité d'une démonstration plus simple de la loi des grands nombres.


    ![Galton](../images/Galton.png){align=right width=40%} 
    Au début du XIXe siècle, la modélisation des erreurs de mesure va devenir centrale pour faire de la statistique une science à part entière.
    Lagrange et Laplace développent une approche probabiliste de la théorie des erreurs. Gauss (1809, 1821), après Legendre (1805), imagine une
    méthode des moindres carrés qu'il applique avec succès à la prédiction de la position d'un astéroïde. Il y propose de comprendre l'écart-type
    comme une « erreur moyenne à craindre ». L'introduction de méthodes statistiques en sociologie est l'oeuvre du mathématicien et astronome
    belge Quételet dans les années 1830. Il réfléchit à la distribution de données autour de la moyenne, ce qui sera approfondi notamment par
    l'Anglais Galton.

    [Un lien vers une animation sous GeoGebra de la planche de Galton](https://www.geogebra.org/m/CHV793HY){target=_blank}

## Rappels de première  

### Variable aléatoire discrète

??? faq "À titre d'exemple"
    === "Exemple 0 : "
        Un jeu consiste à lancer deux fois de suite une pièce de monnaie équilibrée.  
        On gagne $5$ € à chaque fois qu'on obtient pile et on perd $2$ € à chaque fois qu'on obtient face.  
        Soit $X$ la variable aléatoire qui, à chaque partie, associe le gain, éventuellement négatif, obtenu à la fin.  
        1. Déterminer l'ensemble $\Omega$ des gains possibles.  
        2. Déterminer la loi de probabilités de $X$ ; la présenter sous forme de tableau.  
        3. Déterminer l'espérance de $X$ ; comment interpréter ce résultat ?

!!! tip "Définition"
    Définir une variable aléatoire discrète $X$ pour une expérience aléatoire d'univers $\Omega$ revient à associer à chaque issue de $\Omega$ un nombre réel.  
    La loi de probabilité de $X$ associe à chaque valeur prise par $X$ une probabilité :

    $$
    \begin{array}{|l|*3{c|}}\hline
    \rule[-1ex]{0pt}{4ex} \text{Valeur prise } x_i & x_1 & \ldots & x_n \\\hline
    \rule[-1ex]{0pt}{4ex} \text{Probabilité } p_i  & p_1 & \ldots & p_n \\\hline
    \end{array}
    $$

    **L'espérance de $X$** est : $\boxed{E(X) = p_1 x_1 + \ldots + p_n x_n = \displaystyle \sum_{i=1}^{n} p_i \times x_i}$

    La loi des grands nombres (qui sera vue plus tard dans l'année) affirme qu'en répétant « suffisamment » de fois l'expérience aléatoire, la moyenne des valeurs prises par la variable aléatoire $X$ sera « proche » de $E(X)$.

    **La variance de $X$** est : $\boxed{V(X) = \displaystyle \sum_{i=1}^{n} p_i \times \left(x_i - E(X)\right)^2 = E\left(X^2\right)-\left(E(X)\right)^2}$  

    **L'écart-type de $X$** est : $\boxed{\sigma(X) = \sqrt{V(X)}}$

??? faq "À titre d'exemple"
    === "Exemple 1 : "
        La loi de probabilité de $X$ associe à chaque valeur prise par $X$ une probabilité :

        $$
        \begin{array}{|l|*5{c|}}\hline
        \rule[-1ex]{0pt}{4ex} \text{Valeurs } x_i & -1 & 0 & 1 & 2 & 3 \\\hline
        \rule[-1ex]{0pt}{4ex} \text{Probabilité } p_i  & 0,2 & 0,3 & 0,25 & 0,15 & 0,1 \\\hline
        \end{array}
        $$

        1) Calculer l'espérance de $X$ ;  
        2) Calculer la variance de $X$ de deux façons puis l'écart-type de $X$.  
        3) La fonction $\texttt{simulation_loi()}$ permet de simuler la variable aléatoire $X$. Compléter les pointillés et exécuter la fonction plusieurs fois.

        ```python
        from random import random
        def simulation_loi() :
            x = random()
            if x < 0.2 :
                return -1
            elif x < 0.5 : #elif : sinon si
                return ...
            elif x < ... :
                return 1
            elif x < ... :
                return ...
            else :
                return ...
        ```
        
        4) La fonction suivante renvoie la liste de n (argument de la fonction) simulations de la variable aléatoire $X$ :  

        ```python
        def repetition_loi(n):
            """
            liste en compréhension : on répète n fois simulation_loi
            et les résultats sont ajoutés progressivement à la liste
            """
            return [simulation_loi() for i in range(n)]
        ```  

        5) Cette dernière fonction renvoie la moyenne des valeurs obtenues lors de n (argument de la fonction) simulations de la variable aléatoire $X$ :  

        ```python
        def moyenne_loi(n) :
            """
            en sortie de la boucle Pour, S est la somme
            des résultats obtenus lors des n simulations
            """
            L = repetition_loi(n)
            # liste de n simulations de la loi
            S = 0
            for i in range(n):
                S += L[i]
            return S/n
        ``` 

        {{ IDE('simu1') }}


!!! note "Entraînement Labomep"
    === "Exercice 1: "
        [Simuler une expérience à 2 issues (Python).](https://bibliotheque.sesamath.net/public/voir/5d2e0306b0b0e021d8bbd49b){target=_blank}

    === "Exercice 2: "
        [Loi de probabilité.](https://bibliotheque.sesamath.net/public/voir/5d35ad5fb0b0e021d8bbd4a0){target=_blank}

    === "Exercice 3: "
        [Espérance et écart-type.](https://bibliotheque.sesamath.net/public/voir/5d35c3c8b0b0e021d8bbd4a2){target=_blank}

    === "Exercice 4: "
        [Simuler une variable aléatoire (Python).](https://bibliotheque.sesamath.net/public/voir/5d35adb2b0b0e021d8bbd4a1){target=_blank}

### Probabilités conditionnelles

!!! tip "Définition"
    Soient $A$ et $B$ deux événements d'un même univers $\Omega$ tel que $P(A) \neq 0$.  
    La probabilité conditionnelle de l'événement $B$ **sachant que l'événement $A$ est réalisé** se note $P_A(B)$ et se définit par :  

    $$P_A(B) = \dfrac{P\left(A \cap B\right)}{P(A)}$$

    De plus, si $P(B) \neq 0$, La probabilité de $A$ sachant $B$ est donc : $P_B(A) = \dfrac{P\left(A \cap B\right)}{P(B)}$  

    $P_A(B) \times P(A) = P_B(A) \times P(B)$ , les deux expressions sont égales à $P\left(A \cap B\right)$ .

!!! note "Recherche"
    === "Exercice 5: "
        $85 \%$ d'une population est vaccinée contre une maladie. On a constaté que $2 \%$ des individus vaccinés n'ont pas été immunisés contre cette maladie.  
        Quelle est la probabilité qu'un individu soit vacciné et non immunisé ?

    === "Exercice 6: "
        Une usine produit des articles en grande quantité, dont certains sont défectueux à cause de deux défauts possibles, un défaut de fabrication ou un défaut d'emballage.  
        Une étude statistique a permis de constater que $12 \%$ des articles sont défectueux, $6 \%$ des articles ont un défaut de fabrication et $8 \%$ des articles ont un défaut d'emballage.  

        1. Représenter la situation avec un tableau à double entrée.  
        2. Un article choisi au hasard présente un défaut d'emballage. Quelle est la probabilité qu'il ait aussi un défaut de fabrication ?

!!! abstract "Propriété 1 ▶ partition et probabilité totale"
    ![Partition](../images/Partition.png){align=left width=30%} 
    Si $A_1 , A_2 , \ldots A_n$ constituent une partition de $\Omega$ , la formule des probabilités totales s'écrit :

    $$P(B) = \displaystyle \sum_{i=1}^{n} P\left(A_i \cap B\right) = \displaystyle \sum_{i=1}^{n} P_{A_i}(B) \times P\left(A_i\right)$$

!!! note "Recherche"
    === "Exercice 7: "
        Pour fabriquer un objet, un artisan constitue un stock important d'un certain type de pièces auprès de trois fournisseurs : Fournier, Guillaume et Hector.   
        La moitié du stock provient de Fournier, $30 \%$ de Guillaume et le reste d'Hector.  
        La proportion de pièces défectueuses est de $1 \%$ chez Fournier, de $2 \%$ chez Guillaume et de $5 \%$ chez Hector.  
        On prélève au hasard une pièce dans le stock.  
        a. Représenter la situation à l'aide d'un arbre pondéré. (On notera $D$ : « la pièce est défectueuse », $F$ : « la pièce vient de Fournier », etc...)  
        b. À l'aide de l'arbre, montrer que la probabilité que la pièce soit défectueuse est donnée par le calcul $0,5 \times 0,1 + 0,3 \times 0,02 + 0,2 \times 0,05$.

!!! abstract "Propriété 2"
    $A$ et $B$ sont deux événements d'une même expérience aléatoire d'un univers $\Omega$, et $P$ est une probabilité définie sur $\Omega$.  

    $A$ et $B$ sont dits **indépendants** si la réalisation de $A$ n'a pas d'influence sur la probabilité de $B$ (c'est-à-dire que $P(B) = P_A(B)$ ) et si la réalisation de $B$ n'a pas d'influence sur la probabilité de $A$ 
    (c'est-à-dire que $P(A) = P_B(A)$ )  

    $A$ et $B$ sont deux événements indépendants si, et seulement si :  

    $$P\left(A \cap B\right) = P(A) \times P(B) \Leftrightarrow P_A(B) = P(B) \text{ si } P(A) \neq 0 \Leftrightarrow P_B(A) = P(A) \text{ si } P(B) \neq 0$$  

    Si deux événements $A$ et $B$ sont indépendants, alors il en est de même pour les événements $\overline{A}$ et $B$, pour $A$ et $\overline{B}$ ainsi que pour $\overline{A}$ et $\overline{B}$ .

!!! note "Recherche"
    === "Exercice 8: "
        Dans une population, un individu est atteint par la maladie $m_1$ avec la probabilité $0,05$ et par la maladie $m_2$ avec la probabilité $0,14$.  
        On choisit un individu au hasard dans cette population. On admet que les événements $M_1$ : « l'individu a la maladie $m_1$ » et $M_2$ : « l'individu a la maladie $m_2$ » sont indépendants.  
        a. Quelle est la probabilité de l'événement $E$ : « l'individu a contracté au moins l'une des deux maladies » ?  
        b. Présenter dans un tableau les probabilités des événements $M_1 \cap M_2$ ; $M_1 \cap \overline{M_2}$ ; $\overline{M_1} \cap M_2$ et $\overline{M_1} \cap \overline{M_2}$ .  
        c. Déterminer la probabilité de l'événement $F$ : « l'individu présente une seule de ces maladies ».

!!! note "Entraînement Labomep"
    === "Exercice 9: "
        [Probabilité conditionnelle et tableau à double entrée.](https://bibliotheque.sesamath.net/public/voir/5d1a6e2eb0b0e021d8bbd141){target=_blank}

    === "Exercice 10: "
        [Probabilité conditionnelle et arbre.](https://bibliotheque.sesamath.net/public/voir/91424){target=_blank}

    === "Exercice 11: "
        [Probabilité totale (niveau 1).](https://bibliotheque.sesamath.net/public/voir/91420){target=_blank}

    === "Exercice 12: "
        [Probabilité totale (niveau 2).](https://bibliotheque.sesamath.net/public/voir/5ceab8f47051c47ef6caf765){target=_blank}

    === "Exercice 13: "
        [Suite arithmético-géométrique et probabilités conditionnelles.](https://bibliotheque.sesamath.net/public/voir/91426){target=_blank}
