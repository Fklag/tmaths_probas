
![Arbre1](../images/Arbre1.png){align=right width=40%} 
### Succession d'épreuves quelconques

On peut représenter une succession d'épreuves quelconques par un arbre pondéré.  

Dans l'arbre ci-contre, le chemin de $A$ suivi de $B$ représente l'intersection des événements $A$ et $B$ et ainsi de suite ....

Les probabilités portées sur les branches du second niveau sont des **probabilités conditionnelles**.

??? faq "À titre d'exemple 2"
    === "premier exemple : "
        Une urne contient $3$ boules rouges et $5$ boules bleues. On tire successivement (sans remise) deux boules de l'urne. On note les événements :  
        • $B$ : « La boule tirée est bleue ».  
        • $R$ : « La boule tirée est rouge ».  

        L'ensemble des issues est l'ensemble $\Omega  = \left\{(R, R); (R, B); (B, R); (B, B)\right\}$

        ![Arbre2](../images/Arbre2.png){align=center} 

    === "deuxième exemple : "
        Avec la même urne, on tire successivement avec remise deux boules. Déterminer la loi de probabilité sur l'ensemble $\Omega$ .

### Succession d'épreuves indépendantes

**En guise d'explications** : Dans le premier exemple ci-dessus, le résultat du deuxième tirage est conditionné par le résultat du premier :  
on dit que les deux tirages ne sont pas indépendants.  
En revanche, dans le deuxième exemple, le résultat du deuxième tirage n'est pas conditionné par le résultat du premier : on dit que les deux tirages sont indépendants.  
En notant $P(R)$ et $P(B)$ les probabilités respectives d'obtenir une boule rouge et une boule bleu, on constate que :  

$$P(R, R) = P(R)^2 \, ; \, P(B, B) = P(B)^2 \, ; \, P(B, R) = P(R, B) = P(B) \times P (R)$$

!!! tip "Définition"
    Deux épreuves successives sont **indépendantes** lorsque le résultat de la première n'influe pas sur le résultat de la seconde.  
    Si on considère deux événements $A$ et $B$ indépendants tel que $P(A) \neq 0$, la probabilité de $B$ sachant $A$ est égale à la probabilité de $B$ puisque l'événement $A$ n'a pas d'influence sur l'événement $B$.  
    Cela justifie que $P_A(B) = P(B)$ puis que $P\left(A \cap B\right) = P(A) \times P(B)$.  

    On peut représenter une succession de $n$ épreuves indépendantes par un **arbre pondéré** à $n$ niveaux.  
    On considère $n$ épreuves indépendantes ayant pour univers respectifs $\Omega_1 , \Omega_2 , \ldots , \Omega_n$ . La succession de ces $n$ épreuves indépendantes génère une épreuve dont les issues 
    sont des listes $(\omega_1 , \ldots , \omega_n)$, où $\omega_i$ est un élément de $\Omega_i$ .

### Épreuve de Bernoulli, loi de Bernoulli

!!! tip "Définition"
    On appelle **épreuve de Bernoulli** une expérience aléatoire possédant exactement **deux issues contraires**.  
    On peut considérer l'une des issues comme étant **un succès S** et l'autre **un échec E**.

    On note $p$ la probabilité d'un succès, si bien que la probabilité de l'échec est égale à $1 - p$ .  
    On définit la variable aléatoire $X$ qui associe à chaque réalisation de cette expérience aléatoire :  

    **la valeur $1$ en cas de succès.**  
    **la valeur $0$ en cas d'échec.**  

    La variable aléatoire $X$ suit la loi dite de **Bernoulli de paramètre $p$**. On note $X \sim \mathcal{B}(p)$ .  
    En tant que loi discrète (on peut compter les valeurs prises par $X$ ), la loi de Bernoulli de paramètre $p$ se résume dans le tableau suivant :

    ![Arbre3](../images/Arbre3.png){align=center} 

!!! abstract "Propriété 3"
    Si $X$ suit la loi de Bernoulli de paramètre $p$, alors l'espérance de $X$, la variance et l'écart-type sont donnés par :  
    • $E(X) = p \qquad$ • $V(X) = p(1 - p) \qquad$ • $\sigma(X) = \sqrt{p(1 - p)}$

    *Démonstrations laissées en exercice*

??? danger "Algorithme et Python 1 ▶ Modélisation de la variable aléatoire $X : X \sim \mathcal{B}(p)$"
    Exécution du programme « bernoulli ».

    ```pycon
    >>> bernoulli(1)
    1
    >>> bernoulli(0.8)
    1
    >>> bernoulli(0.8)
    0
    >>> bernoulli(0)
    0
    ```

    === "Codage en Python : "

        {{ IDE('bernoulli') }}

### Schéma de Bernoulli

!!! tip "Définition"
    On appelle **schéma de Bernoulli de paramètres $n$ et $p$** la répétition de $n$ épreuves de Bernoulli **identiques** et **indépendantes** ; chacune des épreuves de Bernoulli a deux issues contraires
     **succès** $(S)$ et **échec** $(E)$, le succès ayant la probabilité $p$ d'être réalisé.  

    Une issue de cette expérience aléatoire est une liste de $n$ lettres (n-uplet) prises parmi $S$ et $E$ .  
    Par exemple : $\underbrace{(S, E, S, \ldots , E, S)}_{n \text{ lettres }} , \underbrace{(E, E, S, \ldots , S, S)}_{n \text{ lettres }}$ sont des issues possibles.

    L'univers de cette expérience aléatoire est donc $\Omega = \left\{S; E\right\}^n$ .

    ![Arbre4](../images/Arbre4.png){align=center} 

!!! note "Recherche"
    === "Exercice 14: "
        1. Représenter avec un arbre le schéma de Bernoulli de paramètres 2 et p, compléter le tableau.  
           Entourer en rouge les issues avec un seul succès.

        $$
        \begin{array}{*3{|c|}}\hline
        \rule[-1ex]{0pt}{4ex} \text{Nombres d'issues à } 0 \text{ succès } & \text{Nombres d'issues à } 1 \text{ succès } & \text{Nombres d'issues à } 2 \text{ succès } \\\hline
        \rule[-1ex]{0pt}{4ex}  &  & \\\hline
        \end{array}
        $$
        
        2. Représenter avec un arbre le schéma de Bernoulli de paramètres $3$ et $p$, compléter le tableau.  
           Entourer en rouge les issues avec un seul succès, en vert celles avec deux succès, en bleu celles avec trois succès.

        $$
        \begin{array}{*4{|c|}}\hline
        \rule[-1ex]{0pt}{4ex} \text{Nombres d'issues à } 0 \text{ succès } & \text{Nombres d'issues à } 1 \text{ succès } & \text{Nombres d'issues à } 2 \text{ succès } & \text{Nombres d'issues à } 3 \text{ succès }\\\hline
        \rule[-1ex]{0pt}{4ex}  &  &  & \\\hline
        \end{array}
        $$

        3. Représenter avec un arbre le schéma de Bernoulli de paramètres $4$ et $p$, compléter le tableau.  
           Entourer en rouge les issues avec un seul succès, en vert celles avec deux succès, en bleu celles avec trois succès, en orange celles avec quatre succès.

        $$
        \begin{array}{*5{|c|}}\hline
        \rule[-1ex]{0pt}{4ex} \text{Nombres d'issues à } 0 \text{ succès } & \text{Nombres d'issues à } 1 \text{ succès } & \text{Nombres d'issues à } 2 \text{ succès } & \text{Nombres d'issues à } 3 \text{ succès } & \text{Nombresd'issues à } 4 \text{ succès }\\\hline
        \rule[-1ex]{0pt}{4ex}  &  &  &  & \\\hline
        \end{array}
        $$

### Loi binomiale $\mathcal{B}(n, p)$

!!! tip "Définition"
    On considère **un schéma de Bernoulli** de paramètres $n$ ( nombre de répétitions ) et $p$ ( probabilité de succès lors d'une tentative ).  
    **La variable aléatoire $Y$** qui associe à chaque issue de cette expérience aléatoire le nombre de succès suit la loi binomiale de paramètres $n$ et $p$.  
    **La loi binomiale de paramètres $n$ et $p$** est notée $\mathcal{B}(n, p)$ .  
    On écrit $Y \sim \mathcal{B}(n, p)$ pour dire que $Y$ suit la loi binomiale de paramètres $n$ et $p$ .

!!! abstract "Propriété 4"
    Soit $Y$ une variable aléatoire telle que $Y \sim \mathcal{B}(n, p)$ .  
    Soit $k$ un nombre entier compris entre $0$ et $n$ . La probabilité d'obtenir $k$ succès parmi les $n$ répétitions indépendantes et identiques est :

    $$P(Y = k) = \binom{n}{k} \times p^k \times (1-p)^{n-k}$$

    *Pour comprendre d'où vient la formule et bien comprendre la loi binomiale, n'hésitez pas à suivre* [la vidéo de Hans Amble](https://www.youtube.com/watch?v=dJaEp4rKfAs){target=_blank} .

!!! abstract "Méthode"
    L'usage de la calculatrice permet d'obtenir $P(X = k)$ et aussi $P(X \leqslant k)$ directement. Cette utilisation sera privilégiée avec des paramètres « assez grands ».  

    • Pour obtenir $P(X \geqslant k)$ (où $k$ est un entier naturel), on pourra utiliser la formule suivante :

    $$P (X \geqslant k) = 1 - P (X \leqslant k - 1)$$

    • Pour obtenir $P(a \leqslant X \leqslant b)$ (où $a$ et $b$ sont des entiers naturels), on pourra utiliser la formule suivante :

    $$P (a \leqslant X \leqslant b) = P (X \leqslant b) - P (X \leqslant a - 1)$$

!!! note "Recherche"
    === "Exercice 15: "
        Une entreprise fabrique, en grande quantité, des pièces métalliques rectangulaires dont les cotes sont exprimées en millimètres. Un contrôle de qualité consiste à vérifier que la longueur 
        et la largeur des pièces sont conformes à la norme en vigueur.  
        **Dans ce qui suit, tous les résultats approchés seront arrondis à $10^{-3}$** .

        On note $E$ l'événement : « une pièce prélevée au hasard dans le stock de l'entreprise est conforme ». On suppose que la probabilité de l'événement $E$ est $0,9$.  
        On prélève au hasard 100 pièces dans le stock. Le stock est assez important pour que l'on puisse assimiler ce prélèvement à un tirage avec remise de 100 pièces. On considère la variable aléatoire $X$ qui, à tout prélèvement de 100 pièces, associe le nombre de pièces conformes parmi ces 100 pièces.  

        1. Justifier que la variable aléatoire X suit une loi binomiale. On déterminera les paramètres.  
        2. Calculer la probabilité que, dans un tel prélèvement, exactement 93 pièces soient conformes.  
        3. Déterminer l'espérance de la variable aléatoire X. Interpréter ce résultat.  
        4. Déterminer la probabilité que, dans un tel prélèvement, au plus 85 pièces soient conformes.  
        5. Calculer la probabilité que, dans un tel prélèvement, 88 pièces au moins soient conformes.  
        6. Calculer la probabilité que, dans un tel prélèvement, entre 87 pièces et 93 pièces soient conformes.

??? faq "À titre d'exemple"
    === "Exemple : "
        On jette un dé cubique équilibré 20 fois de suite et on appelle $X$ la variable aléatoire qui dénombre les 6 obtenus lors de ces 20 lancers.  
        En utilisant un tableur, on obtient :

        ![tableur1](../images/tableur1.png){align=left width=40%} 
        L'expérience aléatoire est assimilable à 20 répétitions identiques et indépendantes d'une même épreuve de Bernoulli.  

        Le succès $S$ :« Avoir un 6 sur la face supérieure du dé » est réalisé avec la probabilité $p = \dfrac{1}{6}$ et l'échec $S$ avec la probabilité $q = 1 - p = \dfrac{5}{6}$ .
        
        On a donc un schéma de Bernoulli de paramètres $n = 20$ et $p = \dfrac{1}{6}$ .
        
        La variable aléatoire $X$ qui dénombre les succès de l'expérience aléatoire suit donc une loi binomiale de paramètres $n = 20$ et $p = \dfrac{1}{6}$ : $X \sim \mathcal{B}\left(20, \dfrac{1}{6}\right)$ .

        On peut représenter la loi de probabilité de $X$ par un diagramme en bâtons, la hauteur de chaque bâton étant proportionnelle à $P(X = k)$, $0 \leqslant k \leqslant 20$ .

        ![binom1](../images/binom1.png){align=center} 

        On lit sur le tableur et sur le diagramme en bâtons la probabilité d'avoir $7$ succès lors des 20 lancers $P(X = 7) \simeq 0,03$ .  
        On peut faire un calcul pour avoir une valeur plus précise ou utiliser la calculatrice :

        $P(X = 7) = \binom{20}{7} \left(\dfrac{1}{6}\right)^7 \times \left(\dfrac{5}{6}\right)^{13} \simeq  0,02588206$

        On lit sur le tableur ou à l'aide de l'histogramme la probabilité d'avoir au plus $4$ succès lors des 20 lancers : $P(X \leqslant 4) \simeq 0,77$ .  
        On peut le calculer :
        
        $P (X \leqslant 4) = \displaystyle \sum_{k=0}^{4} P(X = k) = \displaystyle \sum_{k=0}^{4} \binom{20}{k} \left(\dfrac{1}{6}\right)^k \times \left(\dfrac{5}{6}\right)^{20-k} \simeq 0,7687492$

        **L'espérance** de $X$ vaut $E(X) = np = 20 \times \dfrac{1}{6} = \dfrac{10}{3} \simeq 3,333$
        
        **La variance** de $X$ vaut : $V(X) = np(1 - p) = 20 \times \dfrac{1}{6} \times \dfrac{5}{6} = \dfrac{100}{36} = \dfrac{25}{9} \simeq 2,778$

        **L'écart-type** de $X$ vaut : $\sigma(X) = \sqrt{np(1 - p)} = \sqrt{\dfrac{25}{9}} = \dfrac{5}{3} \simeq 1,667$

        Graphiquement, on remarque que la valeur de l'espérance est au plus près de l'abscisse de la barre la plus haute du diagramme en bâtons.

!!! note "Recherche ▶ Avec des corrigés en vidéo"
    === "Exercice 16: "
        Espérance, variance et écart-type d'une loi binomiale.  
        Un QCM comporte 8 questions. À chaque question, 3 solutions sont proposées ; une seule est exacte.  
        Chaque bonne réponse rapporte $0,5$ point. $X$ est la variable aléatoire qui compte le nombre de bonnes réponses.  

        1. Quelle note peut-on espérer obtenir en répondant au hasard ? [Un corrigé](https://www.youtube.com/watch?v=95t19fznDOU){target=_blank} .  
        2. Calculer la variance et l'écart-type. [Un corrigé](https://www.youtube.com/watch?v=MvCZw9XIZ4Q&t=30s){target=_blank} .

    === "Exercice 17: "
        Espérance, variance et écart-type d'une loi binomiale.  
        Une expérience aléatoire consiste à lancer cinq fois un dé tétraédrique équilibré dont les faces sont numérotées de 1 à 4. un lancer est gagnant si le 4 est sur la face cachée.  
        On appelle $X$ la variable aléatoire qui associe à chaque issue de l'expérience le nombre de lancers gagnants.  

        1. Montrer que $X$ suit une loi binomiale dont on précisera les paramètres.  
        2. Déterminer la probabilité d'obtenir un seul lancer gagnant.  
        3. Calculer l'espérance, la variance et l'écart-type de la loi de probabilité de $X$.

        Pour une correction, visionner [la vidéo d'Yvan Monka](https://www.youtube.com/watch?v=W98SSzPSAtQ){target=_blank} .

    === "Exercice 18: "
        On tire 4 fois de suite avec remise une boule de l'urne ci-dessous.  
        ![urne1](../images/urne1.png){align=right width=40%} 

        Les boules G sont les boules gagnantes, les P sont les perdantes.  
        On appelle $X$ la variable aléatoire qui compte le nombre de tirages gagnants.  

        Calculer $P(X = 3)$ .

        Pour vous aidez, ci-joint [une correction d'Yvan Monka en vidéo.](https://www.youtube.com/watch?v=1gMq2TJwSh0){target=_blank} .

    === "Exercice 19: "
        **Utiliser un schéma de Bernoulli**  

        Les 13 lettres du mot « MATHEMATIQUES » sont mises dans un sac opaque. On tire successivement, au hasard et avec remise trois lettres du sac. On compte le nombre de consonnes piochées.  

        1. Justifier que cette situation est modélisée par un schéma de Bernoulli, puis représenter l'expérience par un arbre pondéré.  
        2. Déterminer les probabilités :  
           a) de ne piocher aucune consonne ; au moins une consonne.  
           b) de piocher exactement 2 voyelles parmi les 3 lettres piochées. 

!!! note "Entraînement Labomep"
    === "Exercice 20: "
        [Loi binomiale (1).](https://bibliotheque.sesamath.net/public/voir/5e4954fb11bf937495a7e743){target=_blank}

    === "Exercice 21: "
        [Loi binomiale (2).](https://bibliotheque.sesamath.net/public/voir/5e496b3fdaae217496d7557a){target=_blank}

    === "Exercice 22: "
        [Probabilité et loi binomiale.](https://bibliotheque.sesamath.net/public/voir/5e4ef725a183d1135dac99aa){target=_blank}

    === "Exercice 23: "
        [Inverse de la loi binomiale (1).](https://bibliotheque.sesamath.net/public/voir/5e4f0498a183d1135dac99ab){target=_blank}

    === "Exercice 24: "
        [Inverse de la loi binomiale (2).](https://bibliotheque.sesamath.net/public/voir/5e4f04d3a183d1135dac99ac){target=_blank}

!!! note "Recherche"
    === "Exercice 25: "
        **questions flash (développement des automatismes)**    
        $37 \%$ des pratiquants d'une salle de remise en forme restent plus de deux heures dans l'établissement.  
        Pour 20 personnes qui arrivent dans la salle la première heure, la variable aléatoire $X$ dénombre ceux qui resteront plus de deux heures.

        ![tableur2](../images/tableur2.png){align=left width=40%} 
        1. Justifier que $X \sim \mathcal{B}(20~;~ 0,37)$   
        2. **Lecture du tableur :**  
           La loi de $X$ est représentée dans le tableau ci-contre.  
           a) Lire dans le tableau $P(X = 9)$ . Interprétez ce résultat.  
           b) Lire dans le tableau $P(X \leqslant 6)$ . Interprétez ce résultat.  
           c) Lire dans le tableau $P(4 \leqslant X \leqslant 9). Interprétez ce résultat.  
           d) Déterminer la plus grande valeur $k_1$ telle que $P(X \leqslant k_1 ) \leqslant 0,025$ . Interprétez ce résultat.  
           e) Déterminer la plus petite valeur $k_2$ telle que $P(X \leqslant k_2 ) \geqslant 0,975$ . Interprétez ce résultat.  
           f) Déduire des deux questions précédentes deux nombres entiers $a$ et $b$ telle que $P(a \leqslant X \leqslant b) \geqslant 0,95$ . Interprétez ce résultat.

    === "Exercice 26: "
        Pour embaucher ses cadres une entreprise fait appel à un cabinet de recrutement. La procédure retenue est la suivante. Le cabinet effectue une première sélection de candidats sur dossier.  
        $40 \%$ des dossiers reçus sont validés et transmis à l'entreprise. Les candidats ainsi sélectionnés passent un premier entretien à l'issue duquel $70 \%$ d'entre eux sont retenus.  
        Ces derniers sont convoqués à un ultime entretien avec le directeur des ressources humaines qui recrutera $25 \%$ des candidats rencontrés.

        1. On choisit au hasard le dossier d'un candidat. On considère les évènements suivants :  
           • $D$ : « Le candidat est retenu sur dossier »;  
           • $E_1$ : « Le candidat est retenu à l'issue du premier entretien »;  
           • $E_2$ : « Le candidat est recruté ».  

           a) Reproduire et compléter l'arbre pondéré ci-dessous.
           ![Arbre5](../images/Arbre5.png){align=center} 

           b) Calculer la probabilité de l'évènement $E_1$ .  
           c) On note $F$ l'évènement « Le candidat n'est pas recruté ».  
              Démontrer que la probabilité de l'évènement $F$ est égale à $0,93$.  
        
        2. Cinq amis postulent à un emploi de cadre dans cette entreprise. Les études de leur dossier sont faites indépendamment les unes des autres. On admet que la probabilité que chacun d'eux soit recruté est égale à $0,07$ .  
           On désigne par $X$ la variable aléatoire donnant le nombre de personnes recrutées parmi ces cinq candidats.

           a) Justifier que $X$ suit une loi binomiale et préciser les paramètres de cette loi.  
           b) Calculer la probabilité que deux exactement des cinq amis soient recrutés. On arrondira à $10^{-3}$ .
        
        3. Quel est le nombre minimum de dossiers que le cabinet de recrutement doit traiter pour que la probabilité d'embaucher au moins un candidat soit supérieure à $0,999$ ?

### Approfondissement

On considère une variable aléatoire $X$ dont on connaît la loi de probabilité. On donne ici plusieurs fonctions écrites en `Python` qui retournent l'espérance et l'écart-type de $X$ .  
Tester les différents programmes en utilisant la plateforme [Jupyter](https://mybinder.org/v2/gh/PHILMAG1/MAGPHI/d82c97d19e29ebe9feac17fad9f1a6de271a532e?filepath=Succession%20epreuves.ipynb){target=_blank} .

```python
#E pour espérance d'une variable aléatoire
#Valeurs et probas sont des listes qui déterminent la loi de probabilité
def esperance(valeurs,probas): 
    mu=sum([valeurs[k]*probas[k] for k in range(len(valeurs))])
    return mu
    
#écart-type d'une variable aléatoire 
#Valeurs et probas sont des listes qui déterminent la loi de probabilité
# var désigne le calcul de la variance.
# deux fonctions sigma1 et sigma2 pour montrer qu'il est possible d'utiliser 
#les deux formules de la variance (nommée var)
def sigma1(valeurs,probas): 
    mu=esperance(valeurs,probas)
    var=sum([(valeurs[j]-mu)**2*probas[j] for j in range(len(valeurs))])
    sig=sqrt(var)
    return(sig)
    
def sigma2(valeurs,probas):
    mu=esperance(valeurs,probas)
    var=sum([(valeurs[j]**2*probas[j]) for j in range(len(valeurs))])-mu**2
    sig=sqrt(var)
    return(sig)
```

??? danger "Algorithme et Python"
    ![Galton2](../images/Galton2.png){align=right width=30%} 
    Une bille est lâchée en haut d'une planche de Galton à $n$ rangées de clous ; en bas de la planche, elle est recueillie dans une des cases numérotées de $0$ à $n$.  
    La figure ci-contre illustre cette situation pour une planche à 4 rangées de clous ($n = 4$).  
    L'objectif de cet exercice est de simuler le lâcher de $k$ billes dans une planche à $n$ rangées, puis de comptabiliser les billes dans chaque case.  
    Pour cela, une bille sera repérée par son abscisse entière, initialement nulle, qui augmentera ou diminuera de $1$ à la suite de chaque impact sur un clou.

    1. On considère une planche à $n$ rangées de clous.  
       a) Combien de cases cette planche comporte-t-elle ?  
       b) Quelles sont les abscisses entières possibles qu'une bille peut avoir, une fois tombée dans une case ?  
       c) Chaque case possède un indice, le premier étant $0$. Quelle est l'expression de cet indice en fonction de $x$, l'abscisse entière finale de la bille ?  

    2. En utilisant le lien [Jupyter](https://mybinder.org/v2/gh/PHILMAG1/MAGPHI/d82c97d19e29ebe9feac17fad9f1a6de271a532e?filepath=Succession%20epreuves.ipynb){target=_blank}  compléter la fonction en `Python` `absfinale` 
       qui renvoie l'abscisse finale d'une bille après $n$ impacts, chacun de ces chocs entraînant la bille vers la gauche avec une probabilité $p$ .

    ```python
    from math import*
    from random import*
    import matplotlib.pyplot as plt

    def absfinale(n,p):
        x = ...
        for impact in range(..):
            if ... :
                x = ...
            else:
                x = ...
        return x
    
    def Galton(k,n,p):
        case = [0 for i in range (n+1)]
        for b in range(...):
            x = absfinale(n,p)
            indice = ...
            case[indice] = ...
        return case

    def histo(k,n,p):
        A=[i for i in range(n+1)]
        H=Galton(k,n,p)
        plt.bar(A,H)
        plt.show()
    ```

    3. La fonction `Galton` simule le lâcher de $k$ billes et renvoie une liste du contenu final des cases.  
       a) Quelle est la valeur de la variable case à la ligne 12 ? À quoi sert cette variable ?   
       b) En utilisant le lien [Jupyter](https://mybinder.org/v2/gh/PHILMAG1/MAGPHI/d82c97d19e29ebe9feac17fad9f1a6de271a532e?filepath=Succession%20epreuves.ipynb){target=_blank} compléter la fonction `Galton` .  
       c) Comparer les résultats de `Galton(10000,10,0.5)` et `Galton(10000,10,0.4)` . Était-ce prévisible ?   
       d) Tester par exemple `histo(10000,10,0.4)` .

??? faq "À titre d'exemple ▶ Planche de Galton : loi binomiale et diagramme"
    === "Extrait du site wikipédia : "
        ![Galton3](../images/Galton3.png){align=right width=40%} 
        Une planche de Galton est un dispositif inventé par Sir Francis Galton qui illustre la convergence d'une loi binomiale vers une loi normale.  
        Des clous sont plantés sur la partie supérieure de la planche, de telle sorte qu'une bille lâchée sur la planche passe soit à droite soit à gauche pour chaque rangée de clous.  
        Dans la partie inférieure les billes sont rassemblées en fonction du nombre de passages à gauche et de passage à droite qu'elles ont fait. Ainsi chaque case correspond à un résultat possible d'une expérience binomiale (en tant qu'une expérience de Bernoulli répétée) et on peut remarquer que la répartition des billes dans les cases approche la forme d'une courbe de Gauss, ceci étant d'autant plus vrai que le nombre de rangées augmente, autrement dit : la loi binomiale converge vers la loi normale. Il s'agit donc d'une illustration du théorème de Moivre-Laplace.  
        
        Pour une description et une simulation, on peut aller [sur le site de Thérèse Eveilleau](http://therese.eveilleau.pagesperso-orange.fr/pages/truc_mat/textes/galton.htm){target=_blank} .