# Favoriser et valoriser l'autonomie

Le travail que je présente sur ce site a été mené par Patrick SCHILLI , Philippe MAGOUTIER membres de l'équipe pédagogique du laboratoire de Maths à la Réunion.

| Succession d'épreuves indépendantes | Le chapitre sur le dénombrement |
|:--------------------:|:------:|
| [Télécharger au format .pdf](http://lycee.lagrave.free.fr/maths/term/chapitre_8_succession_epreuves_independantes.pdf){ .md-button } | [Télécharger au format .pdf](http://lycee.lagrave.free.fr/maths/term/chapitre_5_denombrement.pdf){ .md-button } |

## Les documents de travail

De nombreux liens hypertextes qui amènent vers :  
✦ des animations GeoGebra pour émettre des conjectures ou/et comprendre les nouveaux concepts,  
✦ des corrections d'exercices (vidéos),  
✦ des feuilles de programmation en Python sur la plateforme Jupyter,  
✦ des exercices d'entraînement sous Wim's ou LaboMep,  
✦ des sites éducatifs sur l'histoire des mathématiques,  
✦ des feuilles de travail sous Wim's et LaboMep, ainsi que des évaluations formatives sur ces plateformes.

